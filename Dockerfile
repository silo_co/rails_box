FROM ruby:2.2.0
MAINTAINER Tal Moshayov tal@silo.co

# APT-GET
RUN apt-get update -qq
RUN apt-get install -y --no-install-recommends wget sudo zsh openssh-server curl vim git build-essential zip python-pip libxml2-dev libxslt-dev python-dev python-setuptools libxslt1-dev libpq-dev imagemagick libmagickwand-dev postgresql-client telnet
RUN apt-get clean

# external gems
RUN gem install nokogiri -- --use-system-libraries

# PHANTOMJS (for testing)
ADD phantomjs-1.9.8-linux-x86_64.tar.bz2 /usr/local/share/
RUN sudo ln -s /usr/local/share/phantomjs-1.9.8-linux-x86_64/bin/phantomjs /usr/local/share/phantomjs
RUN sudo ln -s /usr/local/share/phantomjs-1.9.8-linux-x86_64/bin/phantomjs /usr/local/bin/phantomjs
RUN sudo ln -s /usr/local/share/phantomjs-1.9.8-linux-x86_64/bin/phantomjs /usr/bin/phantomjs

## POSTGRESQL - only needed for build
RUN apt-get install locales
RUN locale-gen en_US en_US.UTF-8
RUN echo locales locales/default_environment_locale select None | debconf-set-selections
RUN echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections
RUN echo 'LANG="UTF-8"' > /etc/default/locale
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN dpkg-reconfigure -f noninteractive locales
ENV LANG="en_US.UTF-8" LC_CTYPE="en_US.UTF-8" LC_NUMERIC="en_US.UTF-8" LC_TIME="en_US.UTF-8" LC_COLLATE="en_US.UTF-8" LC_MONETARY="en_US.UTF-8" LC_MESSAGES="en_US.UTF-8" LC_PAPER="en_US.UTF-8" LC_NAME="en_US.UTF-8" LC_ADDRESS="en_US.UTF-8" LC_TELEPHONE="en_US.UTF-8" LC_MEASUREMENT="en_US.UTF-8" LC_IDENTIFICATION="en_US.UTF-8"

RUN apt-get install -y postgresql postgresql-contrib
USER postgres
RUN /etc/init.d/postgresql start &&\
    psql --command "CREATE USER silo_build WITH SUPERUSER PASSWORD 'silo_build';" &&\
    createdb -O silo_build silo_build &&\
    psql --command "CREATE EXTENSION hstore;"
USER root

# WERCKER CLI
RUN curl https://s3.amazonaws.com/downloads.wercker.com/cli/stable/linux_amd64/wercker -o /usr/local/bin/wercker
RUN chmod +x /usr/local/bin/wercker

## AWSCLI
RUN pip install awscli

# EB CLI
RUN pip install awsebcli

# OH-MY-ZSH (for the fun of it)
RUN git clone --depth=1 https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
RUN cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
RUN echo "export ZSH=~/.oh-my-zsh" >> ~/.zshrc
RUN echo "export PATH=\"$PATH\"" >> ~/.zshrc
RUN chsh -s $(grep /zsh$ /etc/shells | tail -1)

# PREINSTALLED GEMS (speeding up bundle time)
ADD Gemfile /usr/dev/
ADD Gemfile.lock /usr/dev/
RUN cd /usr/dev/ \
    && bundle install --path /usr/dev/bundle

# EB ENV SETUP TEMPLATES
ADD .aws /root/.aws
ADD .ebextensions /usr/dev/.ebextensions
ADD .elasticbeanstalk /usr/dev/.elasticbeanstalk
ADD Dockerrun.aws.json /usr/dev/

# SILO SCRIPTS
ADD scripts/eb_deploy.sh /usr/dev/
RUN chmod +x /usr/dev/eb_deploy.sh

WORKDIR /usr/dev/silo
EXPOSE 3000
CMD ["rails", "server"]
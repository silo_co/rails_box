source 'http://rubygems.org'

ruby '2.2.0' # locking ruby version to ensure gem compatibility

gem 'omniauth', '~> 1.2.2'
gem 'omniauth-linkedin-oauth2', '~> 0.1.5'
gem 'omniauth-google-oauth2', '~> 0.2.5'
gem 'omniauth-facebook'
gem 'omnicontacts', '~> 0.3.4'
gem 'rack_session_access', '~> 0.1.1'
gem 'gmail', git: 'https://github.com/tal-moshayov/gmail.git'
gem 'bootstrap-typeahead-rails'
gem 'hogan_assets'
gem 'koala'
gem 'linkedin-oauth2', '~> 1.0'

gem 'rake'
gem 'rails', '4.1.6'
gem 'rails-observers'

gem 'bcrypt-ruby', '~> 3.0.1' # Blowfish encryption utils
gem 'pg', '~> 0.18' # PostgreSQL
gem 'sass', '~> 3.2.7'

#gem 'activerecord-postgresql-adapter', '0.0.1'
gem 'devise', '~> 3.2.4' # Authentication
gem 'devise-async', '~> 0.9.0' # sends devise emails asynchroneously (via resque)
gem 'cancancan', '~> 1.8.0' # Authorization

gem 'haml', '~> 4.0.2' # templating engine instead of default erb
gem 'haml-rails', '~> 0.5.3' # makes rails auto-generate views in haml instead of erb
gem 'active_model_serializers', '~> 0.7.0'
gem 'will_paginate', '~> 3.0'
gem 'faker', '~> 1.0.1'
gem 'valid_email', github: 'yossi-shasho/valid_email' # validates email format
gem 'redis', '~> 3.0.2'
gem 'redis-objects', '~> 0.6.1', :require => 'redis/objects' # Map Redis types directly to Ruby objects
gem 'sidekiq', '~> 3.3' # a Redis-backed Ruby library for creating background jobs
gem 'sidekiq-scheduler', '~> 1.0' # support for queueing items in the future.
gem 'sidekiq_mailer', git: 'https://github.com/wispdom/sidekiq_mailer' # pointing to this fork as the master doesn't support sidekiq 3.x (stupid dependency line)
gem 'sidekiq-failures'
gem 'sidekiq-unique-jobs'
gem 'sinatra', '>= 1.3.0', :require => nil # required for sidekiq web interface
gem 'paper_trail', '~> 3.0.2' # Tracks changes to models data
gem 'best_in_place', '~> 3.0.0' #, git: 'https://github.com/yossi-shasho/best_in_place' # In place editing
gem 'gabba', '~> 1.0.1' # send server-side notifications to Google Analytics
gem 'mixpanel', '~> 4.1'
gem 'figaro', '~> 0.7.0' # Simple Rails app configuration
gem 'database_cleaner', '~> 1.2.0'
gem 'rack-ssl-enforcer', '0.2.8', group: :production
gem 'public_suffix', '~> 1.2.0' # parse domain names from URLs
gem 'roadie', '~> 2.4.0' # inlines email styles
gem 'rails_autolink', '~> 1.1.0' # auto links texts
gem 'enumify', '~> 0.1.0' # provides enumerators to models in correlation with active record
gem 'browser' # browser object in view, e.g. <% if browser.ie6? %>
gem 'sanitize' #html sanitizer                 enu
gem 'groupdate' #active records grouping utility
gem 'truncate_html', '~> 0.9.2'
gem 'redirect_on_back'
gem 'nokogiri', '~> 1.5.11'
gem 'rollout', '~> 2.1.0' #Feature flippers
gem 'rollout_ui' #, '0.3.0' #control UI for rollout gem
gem 'activerecord-import', '~>0.4.0'
gem 'connection_pool', '~> 2.0.0' # Generic connection pooling for Ruby
gem 'shortener' # an inner bit.ly-like service
gem 'lograge' # Taming Rails' Default Request Logging
gem 'rack-cors', :require => 'rack/cors'
gem 'gcm'
gem 'houston'

#client side
gem 'bootstrap-sass', '~> 2.3.2.2' # Sass-powered version of Twitter's Bootstrap
gem 'bootstrap_helper', '~> 4.2.3'
gem 'font-awesome-rails', '~> 4.2.0' #an iconic font designed for use with Twitter Bootstrap
gem 'jquery-rails', '~> 3.1.0' # JQuery 1.11
gem 'jquery-ui-rails'
gem 'rails-settings-cached', github: 'yossi-shasho/rails-settings-cached'
gem 'carrierwave', '~> 0.9.0' # file uploads
gem 'fog', '~> 1.20' # Amazon S3 support for carrierwave
gem 'unf' # resolves fog gem errors (http://stackoverflow.com/questions/19666226/warning-with-fog-and-aws-unable-to-load-the-unf-gem)
gem 'rmagick', '2.13.4' # image processing, used by carrierwave to e.g. resize logo images
gem 'nested_form', '~> 0.3.2'
gem 'gmail_xoauth', '0.4.1'
gem 'select2-rails', '~> 3.5.7' # JS token inputs, pretty select boxes and more
gem 'simple_form', '~> 3.0.0' # pretty bootstrap forms
gem 'mini_magick' # for redactor
gem 'htmlentities' # encode and decode HTML
gem 's3_direct_upload' # generate a form that allows to upload directly to Amazon S3. Multi file uploading supported by jquery-fileupload.

gem 'email_reply_parser', '~> 0.5.3' # strip off unneeded parts of a reply email
gem 'thin', require: false # webrick on steroids. not mandating this as production may run other servers (e.g. Passenger)

gem 'possessive' # "Brian".possessive # => Brian's, #Sooners#.possessive # => Sooners'
gem 'indefinite_article' # "apple".indefinitize => "an apple"
gem 'remotipart' # file upload over ajax
gem 'gibbon', '1.1.5'
gem 'fullname-parser'
gem 'turbolinks', '~> 2.0'
gem 'jquery-turbolinks', '~> 2.0'
gem 'nprogress-rails' # ajax progress bars
gem 'mobile-fu' # detect mobile and tablet devices
gem 'factory_girl_rails', '~> 4.1.0', group: [:test, :development, :staging, :demo, :showcase]
gem 'chartkick'
gem 'uglifier', ' ~> 2.4'
gem 'awesome_print' # better console printing (using 'ap')
gem 'neography','1.7.2'


gem 'kmts', '~> 2.0.0' #KissMetrics
gem 'intercom-rails'

gem 'concurrent-ruby'

group :development, :test do
  gem 'rspec-rails', '~> 2.99' # RSpec unit testing
  gem 'rails-erd'
  gem 'byebug'
  #gem 'spring-commands-rspec'
  #gem 'growl'
  #gem 'zeus', '0.13.4.pre2' # replacement for Spork. Make sure you run 'zeus start' in a terminal before running 'guard'
  #gem 'debugger'
  #gem 'zeus-parallel_tests'
end

group :development, :staging, :demo, :showcase do
  gem 'better_errors' #improved exception pages
  gem 'binding_of_caller'
  gem 'meta_request' #for matching chrome extension
end

group :development do
  gem 'bullet'
  gem 'dotenv-rails'
  # gem 'brakeman', :require => false
  gem 'sourceninja', :group => :development
  gem 'quiet_assets' # turn off assets pipeline log
  gem 'flay'
  gem 'capistrano-rails'
end


### START IT GEMS #########
# gem 'sentry-raven'
gem 'newrelic_rpm', '>3.7' #
gem 'simplecov', :require => false, :group => :test # code coverage
gem 'airbrake' # exception trapping
# gem 'errorapp_notifier' # a notifier gem for integrating apps with ErrorApp
gem 'rails_12factor', :group => [:production, :staging, :demo, :showcase] # recommended by Heroku https://devcenter.heroku.com/articles/ruby-support#injected-plugins

gem 'activeadmin', github: 'activeadmin'
# gem 'inherited_resources', github: 'josevalim/inherited_resources', branch: 'rails-4-2'
### END IT GEMS #########


group :test do
  #gem 'spork', '~> 0.9.2'
  gem 'guard', '~> 2.6.1' # automatically run specs when files change
  gem 'guard-rspec', '~> 4.3.0' # automatically run your specs
  gem 'guard-coffeescript', '~> 1.4.0' # automatically compiles CoffeeScripts when files change
  #gem 'guard-jasmine', '~> 1.9.3' # automatically tests your Jasmine specs when files change
  gem 'mock_redis' #in-memory redis implementation

  gem 'capybara', '~> 2.2.1' # UI tests (selenium based)
  gem 'poltergeist', '~> 1.5.0' # phantomJS drivers for rspec & capybara, for JS testing. make sure you have installed phantomJS. @see: http://railscasts.com/episodes/391-testing-javascript-with-phantomjs
  gem 'rb-fsevent' # if RUBY_PLATFORM.downcase.include?("darwin")
  gem 'mocha', '~> 1.0.0', :require => 'mocha/api' # this line resolves a stupid deprecation warning (https://github.com/freerange/mocha/issues/107#issuecomment-16022284)

  gem 'email_spec', '~> 1.4.0'
  gem 'launchy', '~> 2.2.0'
  gem 'rspec-sidekiq', '~> 1.1.0'
end

# Gems used only for assets and not required
# in production environments by default.
gem 'coffee-script-source', '> 1.4.0' # BUGFIX: https://github.com/gregbell/active_admin/issues/1939; 1.5.0 doesn't compile ActiveAdmin JavaScript files
gem 'therubyracer', '~> 0.12'
gem 'sass-rails' #, github: 'rails/sass-rails'

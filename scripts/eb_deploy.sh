#!/bin/bash
if [ -z "$SILO_TARGET_EB_ENV" ]; then
  echo "missing environment variable SILO_TARGET_EB_ENV"
  exit -1
fi
if [ -z "$SILO_AWS_KEY" ]; then
  echo "missing environment variable SILO_AWS_KEY"
  exit -1
fi
if [ -z "$SILO_AWS_SECRET" ]; then
  echo "missing environment variable SILO_AWS_SECRET"
  exit -1
fi
if [ -z "$SILO_DEPLOY_LABEL" ]; then
  echo "missing environment variable SILO_DEPLOY_LABEL"
  exit -1
fi

DEV=/usr/dev
sed -i s/\$SILO_TARGET_EB_ENV/$SILO_TARGET_EB_ENV/g $DEV/.elasticbeanstalk/config.yml
sed -i s/\$SILO_DEPLOY_LABEL/$SILO_DEPLOY_LABEL/g $DEV/Dockerrun.aws.json
sed -i s/\$SILO_AWS_KEY/$SILO_AWS_KEY/g ~/.aws/config
sed -i s/\$SILO_AWS_SECRET/$SILO_AWS_SECRET/g ~/.aws/config

if [ "$SILO_DEBUG" ]; then
  echo "deploying to $SILO_TARGET_EB_ENV"
	echo "config.yml:"; cat $DEV/.elasticbeanstalk/config.yml;
  echo
  echo "Dockerrun:"; cat $DEV/Dockerrun.aws.json;
fi

# eb deploy
cd $DEV
eb deploy